NuRAN Wireless LiteCell 1.5 BSP release notes

Version 2.1.0
NuRAN Wireless


Version 2.1.0 [ 11/09/2018 ]
-------------------------------------------------------------------------------------------------
Additions include:

    - Kernel updated to v4.9.69
    - SystemD updated to v232
    - Added vlan 8021q support 
    - Added Strongswan to Yocto distribution

Bugs that have been addressed in this release include:

    - Corrected watchdog systemd correct operation validation that was not correctly working in some cases
    - Improve processor firmware loading support and corrected random loading instability (DSP)
    - Fixed several ext4 issues (from kernel 4.9.69 addition)
    - Fixed some systemd reliability issues (from systemd 232 addition)
    - Corrected sudo reliability issue by updating to v1.8.20p2
    - The pdtch queue latency from dsp to arm direction could have very big latency randomly
    - Makes sure u-boot load kernel correctly with slower SD card


Pending issues and limitations:

    - None

Version 2.0.0 [ 02/22/2018 ]
-------------------------------------------------------------------------------------------------
Additions include:
    
    - Added support of the OC-2G platform
    - Added an option to limit the throughput while downloading packages
    - Added a method to modify systemd unit files that will be preserved even after a monolitic update
    - Added recovery boot (serial boot) and complete rootfs recovery support
    - Added support of different SD card sizes
    - Added monolithicupdate/initsu download bandwidth limit option
    - Added custom flash user mount configuration for system customization
    - Added openssh key login (without password) and allows user customization in flash
    - Added network console support for u-boot/kernel
    - Added support of custom user/groups/passwords in flash
    - Added support of a new user 'gsm' to be used at login which is a member of group 'sudo'
    - Added security by forcing non root (gsm) account login through ssh/serial
    - Added support of sudo operation through default group sudo(password)/wheel(passwordless)
    - Added support of syslog configuration
    - Added rootfs corruption/modification analysis functionality
    - Added monolithicupdate/initsu image installation pre/post processing scripts support
    - Added password protection to break boot in u-boot
    - Added a protection to disable the serial console input by default in the kernel when not used (controlled by u-boot variable)
    - Added flash configuration automatic backup support and possibility to mount configuration from old backup/reconstructed ram partition. The flash backups are verified /created at each boot and 3h15am every night. Manual flash backups validation/creation could also been done through the checkbk tool
    - Added a script in u-boot to force to recreate SD card partitions in case of corruption
    - mountuser tool used for unit flash configuration has been modified to prepare the flash configuration partition for later analysis with analyzefs tool and will automatically create a backup of this flash partition when considered ok
    - mountuser tool when used for unit flash configuration will displas any flash configuration detected problem automatically on the console
    - Configurable persistent log files (volatile log files are factory default setting)
    - Change the time sync mechanism so it is easier to see if time was sync or not by looking at system date
    - Improved reliability of update per package mechanism

Bugs that have been addressed in this release include:
 
    - Fixed watchdog ticking while repairing rootfs at boot
    - Fixed initsu issue with update log
    - Fixed package update initscripts potential corruption issue
    - Made sure that ntpd restarts in case of any error like a panic condition so system time could be kept in sync

Pending issues and limitations:

    - None


Version 1.1.2.0[ 07/20/2017 ]
-------------------------------------------------------------------------------------------------
Additions include:

	- Added an option to limit the throughput while downloading Yocto packages 
	- Added a method to modify systemd unit files that will be preserved even after a monolitic update
	- Added BSP needed flash configuration softlink for openvpn client/server
	- Added recovery boot (serial boot) and complete rootfs recovery support
	- Added support different SD card sizes
	- Added monolithicupdate/initsu download bandwidth limit option
	- Added Python 3 support

Bugs that have been addressed in this release include:

	- Improved reliability of update per package mechanism
	- Corrected watchdog interrupt fail triggered reboot

Pending issues and limitations:

	- None
	

Version 1.1.1.0[ 05/31/2017 ]
-------------------------------------------------------------------------------------------------
Additions include:

	- None

Bugs that have been addressed in this release include:

	- None

Pending issues and limitations:

	- A reboot can occur when a watchdog interrupt fails to be properly managed during boot

Version 1.1.0[ 05/10/2017 ]
-------------------------------------------------------------------------------------------------
Additions include:

	- VSWR measurement calibration
	- Added 8PSK reduction parameter

Bugs that have been addressed in this release include:

	- Corrected new risks of corruption during monolithic update
	- Fixed memory leaks in the GSM stack

Pending issues and limitations:

	- A reboot can occur when a watchdog interrupt fails to be properly managed during boot
	

Version 1.0.0[ 04/12/2017 ]
-------------------------------------------------------------------------------------------------
Additions include:

	- Updated to Yocto 2.1
	- Updated to Kernel 4.4.32
	- Copies of MLO and u-boot are now located in the /boot folder of the main partition
	- A reliable method to repair and maintain the loader images was implemented
	- Disabled command line saving list of last used commands when rootfs is in R/W
	- Added iproute2 to BSP file system
	- Added lsof debug tool to help figure out what process is writing to rootfs when switch to rw
	- Enabled LED heartbeat to differentiate with power off.
	- The support of shellinabox was abandoned
	- The default config files were modified to officially reflect factory settings
	- A password must now be entered to break uboot. The password is c
	- Basic webmin support was added
	- Support for a new flash chip was added
	- Added opkg alias to force display of allowed commands
	- Added default python packages
	- Improved GMSK transient spectrum due to switching for GSM-850
	- Added support of REV F board
	- Improved RF spectrum in GSM-900
	- Added 8PSK power reduction support
	
Bugs that have been addressed in this release include:

	-	Removed risks of corruption during monolithic update, syncfs and forcerecover
	-	Fixed a monolithic update hang problem by updating to watchdog deamon v5.15
	-	Fixed a possible failure of monolithic update when backup partition is mounted
	-	Fixed DSP loading failures and related possible kernel crash
	-	Limited systemd journal usable space to prevent filling memory
	-	Fixed kernel panic after shutdown command
	-	Fixed AFS and AHS problems related to DTX support
	-	Fixed a power control problem when using AMR/H
	-	Fixed an issue with FACCH with old phones
	-	Fixed AutoTx specific timeslot power tuning
	
Pending issues:

	- A reboot can occur when a watchdog interrupt fails to be properly managed during boot

Version 0.9.3 [ 10/21/2016 ]
-------------------------------------------------------------------------------------------------
Additions include:
- add gdb in the image to help debug
- litecell15fw: fpga version 1.0.12 supports greater clock error (revD only) fix0471
- meta-nrw-bsp: watchdog: add systemd detection to make sure we can wdt reset the system if dead (fix0450)
- meta-nrw-bsp: The 75t FPGA bitstream is now encrypted.
- meta-nrw-bsp: update: add no boot option to help update debugging (fix0625)
- meta-nrw-bsp: update: makes sure the system is bootable (boot files) before rebooting it at update time (fix0628)
- meta-nrw-bsp: add a feature so it is possible to manually force a recover of rootfs from backup partition, overwriting the master rootfs (fix0589)
- meta-nrw-bsp: package update: add a command to display the list of installed packages (fix0595)
- meta-nrw-bsp: systemd: conditionnaly mount the flash configuration partition and fallback to defaultconfig if any problem (fix0555)
- meta-nrw-bsp: setup to read custom hostname from flash memory if available, or use default if not (fix0505)
- meta-nrw-bsp: i2c: added support for i2c-tools (fix0520)

Bugs that have been addressed in this release include:
- meta-nrw-bsp: New DSP version: Fixes Mantis #677. Sends a failure indication when the error
- processor-sdk-linux: rtfifo: added traces to track info about issue0523
- processor-sdk-linux: rtfifo: added traces to track info about issue0523
- meta-nrw-bsp: New DSP version: Fixes Mantis #631. Adds FPGA registers for TRX optimization, New FPGA bitstream version that adds GPS offset count too big error
- meta-nrw-bsp: update: improve error handling when updating package
- u-boot: litecell15: fix u-boot env tools qspi flash partitions definition (fix0532)
- processor-sdk-linux: clkerr: Mantis bug #0000471 Support the new clock fault offset count too big
- processor-sdk-linux: Correction of problem where the virtual address of msgqueue/rtfifo changes between dsp reload and a security verification in these drivers wrongly keep to reload the msgqueue/rtfifo properly, making them unusable (issue 0504)
- meta-nrw-bsp: monolithicupdate: correct the issue while fs corruption could happend because of unattended watchdog reset, each time an update is made (fix0611)
- meta-nrw-bsp: update: correct issue that redo a sync to the backup partition even if it was already manually done (fix0607)
- meta-nrw-bsp: gettime: add dependencies between gettime, ntpd and gpsd to make them boot always in same order (fix0560)
- meta-nrw-bsp: checkboot: when booting on backup partition and a schedule backup partition is still pending, the master was repaired, but the schedule backup update needs also to be removed (fix0627)
- meta-nrw-bsp: update: prepare monolithicupdate for remote use to correctly reports error codes
- meta-nrw-bsp: busybox: unused files cleanup (fix0527)
- meta-nrw-bsp: busybox: added ftpput and ftpget and moves receipe to correct folder(fix0527)
- meta-nrw-bsp: sshpass: added feature to allow use of ssh password through script (fix0549)
- meta-nrw-bsp: openssh: replaced dropbear with openssh (fix0534)
- meta-nrw-bsp: systemd: early remount critical mount to ro at reboot/shutdown/halt to avoid any corruption
- meta-nrw-bsp: systemd: remove not compatible localectl tool and related service (fix0552)
- meta-nrw-bsp: systemd: fix a v229 qa package installation complain with some inst not shipped files
- meta-nrw-bsp: systemd: fix network component that stays in configuration state forever if ipv6 is not supported by the kernel (fix0541)
- meta-nrw-bsp: systemd: added util-linux 2.27 to support systemd 229 on yocto 2.0.1 (fix0533)
- meta-nrw-bsp: systemd: add some init script service unit files to repair compatibility problem with systemd 229 (fix0533)
- meta-nrw-bsp: systemd: update to systemd 229 to fix color output problem (fix0506)
- meta-nrw-bsp: systemd: mask systemd-timedated service and remove related tool timedatectl because it is incompatible in our system (fix0525)
- meta-nrw-bsp: systemd: provides default example files for dhcp and static network configuration
- meta-nrw-bsp: Remove gettime file path not existing in systemd setup to avoid systemd complain at runtime
- meta-nrw-bsp: systemd: mask timesync service because we are already using ntp daemon
- meta-nrw-bsp: gpsd: modification to support ro rootfs with systemd, and setup for systemd service autostart
- meta-nrw-bsp: ntp: modifications to support systemd on ro rootfs
- meta-nrw-bsp: gettime: added support for systemd
- meta-nrw-bsp: Keeps ntpdate from being enable in the rootfs
- meta-nrw-bsp: checkroot: removes unused resetbootcount.service
- meta-nrw-bsp: checkroot: add service file to make sure checkroot runs after mounting "storage".
- meta-nrw-bsp: checkroot: disable mask on checkroot script. Modify checkroot script to be compatible with systemd.
- meta-nrw-bsp: networkd: Change network config links to be compatible with systemd.
- meta-nrw-bsp: fstab: add 'nofail' to prevent emergency mode when mounted device are corrupted.
- meta-nrw-bsp: update-scripts: cosmetic
- meta-nrw-bsp: add LAYERDEPENDS to meta-nrw-bsp
- meta-nrw-bsp: remove login manager configuration, now set in litecell-poky.conf
- meta-nrw-bsp: packagegroup-nrw-core: remove ubi tools, add wget
- meta-nrw-bsp: remove overrides from image recipe
- meta-nrw-bsp: add custom distro configuration
- meta-nrw-bsp: machine: stop building UBI images
- meta-nrw-bsp: systemd: initial commit First working version of litecell15-image with systemd.
- meta-nrw-bsp: lc15-firmware: minor refactoring
- meta-nrw-bsp: libace: refactor recipe
- meta-nrw-bsp: gettime: corrects to make sur RTC updated time is always stored in UTC (fix0529)
- meta-nrw-bsp: shellinabox: fix google package location which has recently changed (fix0542)
- meta-nrw-bsp: package update: fix an issue where the customization of the server to use package update is lost after monolithicupdate (fix0578)
- meta-nrw-bsp: package update: corrects some corruption issue with the update function and prepare it for remote operation (fix0566)

Pending issues:
- None

Version 0.92 [ never released ]
-------------------------------------------------------------------------------------------------
Additions include:
- None

Bugs that have been addressed in this release include:
- Litecell15FW: dsp: Fixes M0000390 M0000391 M0000393 M0000394 issues.
- LC15: Fix missing API header files

Pending issues:
- None

Version 0.91 [ 07/18/2016 ]
-------------------------------------------------------------------------------------------------
Additions include:
- added support for 256m16 ddr3 chip.
- change the way bootcount info is handled.
- support for rmsdet.
- support for vswr.
- add support for 75t type fpga.
- watchdog can not be disabled anymore.
- removed 35t fpga type support.
- New NB & WB filter selection algo.

Bugs that have been addressed in this release include:
- u-boot bug fix mbr repair.
- IQ imbalance fix.

Pending issues:
- None

Version 0.9 [ 05/31/2016 ]
-------------------------------------------------------------------------------------------------
Additions include:
- added some fail prevention feature.
- added automatic time setup at boot time from ntp server.
- added time setup from gps fix after boot.
- added rf output compensation feature.
- added dsp alive feature.
- watchdog daemon is now active by default (and ticks the WDT)

Bugs that have been addressed in this release include:
- corrects usb supply issue.
- corrects i2c bus speed issue.
- correction of some dsp timing issues
- fix dsp/fpga issue for log detector

Pending issues:
- None

Version 0.3 [ 04/11/2016 ]
-------------------------------------------------------------------------------------------------
Additions include:
- 1st preliminary release for LiteCell 1.5 revC board.

Bugs that have been addressed in this release include:
- None

Pending issues:
- None

Version 0.2 [ 02/16/2016 ]
-------------------------------------------------------------------------------------------------
Additions include:
- 1st preliminary release for LiteCell 1.5 revB board.

Bugs that have been addressed in this release include:
- None

Pending issues:
- None

